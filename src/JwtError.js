'use strict'

import autoBind from 'auto-bind'
import ResponseBody from './ResponseBody'

const ERROR_NAME = 'JwtError'
const ERROR_CLASSIFICATION = 'AUTHENTICATION_ERROR'

const CAN_CAPTURE = typeof Error.captureStackTrace === 'function'
const CAN_STACK = !!new Error().stack

export default class JwtError extends Error {
  constructor (error, status, message) {
    const {
      message: errMessage,
      _isJwtError: errIsJwtError,
      msg: errMsg,
      name: errName,
      classification: errClassification,
      statusCode: errStatusCode,
      error: errError,
      stack: errStack
    } = error || {}

    const errorHasKeys = !!Object.keys(error || {}).length
    const finalMessage = message || errMessage || errMsg

    super(finalMessage)

    this._isJwtError = true

    this.name = errName || ERROR_NAME
    this.message = finalMessage
    this.classification = errClassification || ERROR_CLASSIFICATION

    this.statusCode = errStatusCode || 500
    this.error = (errIsJwtError || !errorHasKeys) ? undefined : error


    this.stack = errStack || (
      (CAN_CAPTURE && Error.captureStackTrace(this, JwtError))
      || (CAN_STACK && new Error().stack)
      || undefined
    )

    autoBind(this)
  }

  getResponseBody () {
    const { statusCode, message } = this
    const error = this.toJSON()

    const { NODE_ENV } = process.env
    error.stack = (NODE_ENV === 'production' && undefined) || error.stack

    return new ResponseBody(statusCode, message, undefined, error)
  }

  toJSON () {
    const { toJSON, ...rest } = this
    return JSON.parse(JSON.stringify(rest))
  }
}
