'use strict'

import crypto from 'crypto'
import autoBind from 'auto-bind'
import JwtError from './JwtError'

const JWT_HEADER = {
  alg: 'HS256',
  typ: 'JWT'
}

const JWT_SECRET = 'I\'m Batman!'

const DEFAULT_CONFIG = {
  ENCODING: 'base64'
}

export default class JWT {
  constructor (config = DEFAULT_CONFIG) {
    this.CONFIG = Object.assign({}, DEFAULT_CONFIG, config)

    autoBind(this)
  }

  generate (claims = {}, secret = JWT_SECRET) {
    const { _encode, _hmacSha256, _urlEncode } = this

    let jwtHeader = _encode(JSON.stringify(JWT_HEADER))
    jwtHeader = _urlEncode(jwtHeader)

    let jwtClaims = _encode(JSON.stringify(claims))
    jwtClaims = _urlEncode(jwtClaims)

    let jwtSignature = _hmacSha256((jwtHeader + '.' + jwtClaims), secret)
    jwtSignature = _urlEncode(jwtSignature)

    return [jwtHeader, jwtClaims, jwtSignature].join('.')
  }

  parse (token = '') {
    const { _decode, _urlDecode } = this
    const jwtArray = token && token.split('.')

    if (jwtArray.length !== 3) {
      throw new JwtError(null, 400, 'Invalid Authorization Token')
    }

    let header = _urlDecode(jwtArray[0])
    header = _decode(header)

    let claims = _urlDecode(jwtArray[1])
    claims = _decode(claims)

    const signature = _urlDecode(jwtArray[2])

    try {
      header = JSON.parse(header)
      claims = JSON.parse(claims)
    } catch (e) {
      throw new JwtError(e, 400, 'Invalid Authorization Token')
    }

    return { header, claims, signature }
  }

  validate (token = '', secret = JWT_SECRET) {
    const { _hmacSha256, _urlEncode } = this
    const [header, claims, signature] = token.split('.')

    let hash = _hmacSha256((header + '.' + claims), secret)
    hash = _urlEncode(hash)

    return hash === signature
  }

  _encode (plainText = '', encoding = '') {
    const { CONFIG } = this
    const thisEncoding = encoding || CONFIG.ENCODING
    return Buffer.from(plainText).toString(thisEncoding)
  }

  _decode (cipherText = '', encoding = '') {
    const { CONFIG } = this
    const thisEncoding = encoding || CONFIG.ENCODING
    return Buffer.from(cipherText, thisEncoding).toString('utf8')
  }

  _urlEncode (encoded) {
    const urlEncoded = encoded.replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
    return urlEncoded
  }

  _urlDecode (urlEncoded) {
    let urlDecoded = urlEncoded.replace(/-/g, '+').replace(/_/g, '/')
    while (urlDecoded.length % 4) { urlDecoded += '=' }
    return urlDecoded
  }

  _hmacSha256 (plainText = '', salt = '') {
    const { CONFIG } = this
    const hmac = crypto.createHmac('sha256', salt)
    const hash = hmac.update(plainText, 'utf8').digest(CONFIG.ENCODING)
    return hash
  }
}
