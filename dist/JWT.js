'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _JwtError = _interopRequireDefault(require("./JwtError"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var JWT_HEADER = {
  alg: 'HS256',
  typ: 'JWT'
};
var JWT_SECRET = 'I\'m Batman!';
var DEFAULT_CONFIG = {
  ENCODING: 'base64'
};

class JWT {
  constructor() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : DEFAULT_CONFIG;
    this.CONFIG = Object.assign({}, DEFAULT_CONFIG, config);
    (0, _autoBind.default)(this);
  }

  generate() {
    var claims = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var secret = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : JWT_SECRET;
    var {
      _encode,
      _hmacSha256,
      _urlEncode
    } = this;

    var jwtHeader = _encode(JSON.stringify(JWT_HEADER));

    jwtHeader = _urlEncode(jwtHeader);

    var jwtClaims = _encode(JSON.stringify(claims));

    jwtClaims = _urlEncode(jwtClaims);

    var jwtSignature = _hmacSha256(jwtHeader + '.' + jwtClaims, secret);

    jwtSignature = _urlEncode(jwtSignature);
    return [jwtHeader, jwtClaims, jwtSignature].join('.');
  }

  parse() {
    var token = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var {
      _decode,
      _urlDecode
    } = this;
    var jwtArray = token && token.split('.');

    if (jwtArray.length !== 3) {
      throw new _JwtError.default(null, 400, 'Invalid Authorization Token');
    }

    var header = _urlDecode(jwtArray[0]);

    header = _decode(header);

    var claims = _urlDecode(jwtArray[1]);

    claims = _decode(claims);

    var signature = _urlDecode(jwtArray[2]);

    try {
      header = JSON.parse(header);
      claims = JSON.parse(claims);
    } catch (e) {
      throw new _JwtError.default(e, 400, 'Invalid Authorization Token');
    }

    return {
      header,
      claims,
      signature
    };
  }

  validate() {
    var token = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var secret = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : JWT_SECRET;
    var {
      _hmacSha256,
      _urlEncode
    } = this;
    var [header, claims, signature] = token.split('.');

    var hash = _hmacSha256(header + '.' + claims, secret);

    hash = _urlEncode(hash);
    return hash === signature;
  }

  _encode() {
    var plainText = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var encoding = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var {
      CONFIG
    } = this;
    var thisEncoding = encoding || CONFIG.ENCODING;
    return Buffer.from(plainText).toString(thisEncoding);
  }

  _decode() {
    var cipherText = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var encoding = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var {
      CONFIG
    } = this;
    var thisEncoding = encoding || CONFIG.ENCODING;
    return Buffer.from(cipherText, thisEncoding).toString('utf8');
  }

  _urlEncode(encoded) {
    var urlEncoded = encoded.replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
    return urlEncoded;
  }

  _urlDecode(urlEncoded) {
    var urlDecoded = urlEncoded.replace(/-/g, '+').replace(/_/g, '/');

    while (urlDecoded.length % 4) {
      urlDecoded += '=';
    }

    return urlDecoded;
  }

  _hmacSha256() {
    var plainText = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var salt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var {
      CONFIG
    } = this;

    var hmac = _crypto.default.createHmac('sha256', salt);

    var hash = hmac.update(plainText, 'utf8').digest(CONFIG.ENCODING);
    return hash;
  }

}

exports.default = JWT;