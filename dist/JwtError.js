'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("./ResponseBody"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var ERROR_NAME = 'JwtError';
var ERROR_CLASSIFICATION = 'AUTHENTICATION_ERROR';
var CAN_CAPTURE = typeof Error.captureStackTrace === 'function';
var CAN_STACK = !!new Error().stack;

class JwtError extends Error {
  constructor(error, status, message) {
    var {
      message: errMessage,
      _isJwtError: errIsJwtError,
      msg: errMsg,
      name: errName,
      classification: errClassification,
      statusCode: errStatusCode,
      error: errError,
      stack: errStack
    } = error || {};
    var errorHasKeys = !!Object.keys(error || {}).length;
    var finalMessage = message || errMessage || errMsg;
    super(finalMessage);
    this._isJwtError = true;
    this.name = errName || ERROR_NAME;
    this.message = finalMessage;
    this.classification = errClassification || ERROR_CLASSIFICATION;
    this.statusCode = errStatusCode || 500;
    this.error = errIsJwtError || !errorHasKeys ? undefined : error;
    this.stack = errStack || CAN_CAPTURE && Error.captureStackTrace(this, JwtError) || CAN_STACK && new Error().stack || undefined;
    (0, _autoBind.default)(this);
  }

  getResponseBody() {
    var {
      statusCode,
      message
    } = this;
    var error = this.toJSON();
    var {
      NODE_ENV
    } = process.env;
    error.stack = NODE_ENV === 'production' && undefined || error.stack;
    return new _ResponseBody.default(statusCode, message, undefined, error);
  }

  toJSON() {
    var _this = this,
        {
      toJSON
    } = _this,
        rest = _objectWithoutProperties(_this, ["toJSON"]);

    return JSON.parse(JSON.stringify(rest));
  }

}

exports.default = JwtError;